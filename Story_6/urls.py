from django.urls import path
from . import views
from .views import home, listKegiatan, register, hapus_nama

urlpatterns = [
    path('', home, name='home'),
    path('hasil/', listKegiatan, name='listKegiatan'),
    path('hasil/register/<int:index>/', register, name='register'),
    path('<int:index>/', hapus_nama, name='hapus_nama'),
]
