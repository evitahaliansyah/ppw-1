from django import forms

from .models import Kegiatan, Peserta

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'namaKegiatan',
        ]

class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'Nama_Peserta',
        ]

