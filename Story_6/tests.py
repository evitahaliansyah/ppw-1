from django.test import TestCase, Client
from django.urls import resolve
from .views import home, listKegiatan, register
from .models import Kegiatan, Peserta
from .apps import Story6Config

# Create your tests here.
class TestNamaApp(TestCase):
	def test_nama_app(self):
		self.assertEqual(Story6Config.name, 'Story_6')

class TestKegiatan(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get('/Story_6/hasil/')
		self.assertEqual(response.status_code, 200)

	def test_event_index_func(self):
		found = resolve('/Story_6/hasil/')
		self.assertEqual(found.func, listKegiatan)

	def test_event_using_template(self):
		response = Client().get('/Story_6/hasil/')
		self.assertTemplateUsed(response, 'semuaKegiatan.html')

class TestTambahKegiatan(TestCase):
	def test_add_event_url_is_exist(self):
		response = Client().get('/Story_6/')
		self.assertEqual(response.status_code, 200)

	def test_add_event_index_func(self):
		found = resolve('/Story_6/')
		self.assertEqual(found.func, home)

	def test_add_event_using_template(self):
		response = Client().get('/Story_6/')
		self.assertTemplateUsed(response, 'home.html')

	def test_event_model_create_new_object(self):
		acara = Kegiatan(namaKegiatan="abc")
		acara.save()
		self.assertEqual(Kegiatan.objects.all().count(), 1)

	def test_event_url_post_is_exist(self):
		response = Client().post('/Story_6/', data={'namaKegiatan':'berenang'})
		self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
	def setUp(self):
		acara = Kegiatan(namaKegiatan="abc")
		acara.save()

	def test_regist_url_post_is_exist(self):
		response = Client().post('/Story_6/hasil/', data={'Nama_Peserta':'evita'})
		self.assertEqual(response.status_code, 200)
	
	def test_regist_url_is_exist(self):
		response = Client().get('/Story_6/hasil/')
		self.assertEqual(response.status_code, 200)

# class TestHapusNama(TestCase):
# 	def setUp(self):
# 		acara = Kegiatan(namaKegiatan="abc")
# 		acara.save()
# 		nama = Peserta(Nama_Peserta="cba", Ikut_Kegiatan=acara)
# 		nama.save()
	
	# def test_hapus_url_is_exist(self):
	# 	response = Client().get('/story6/1/')
	# 	self.assertEqual(response.status_code, 200)
