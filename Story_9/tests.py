from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .models import Profile
from .forms import LoginForm, SignupForm
from .views import login_page, signup_page, logout_page, berhasil
from django.apps import apps
from .apps import Story9Config



class LoginAppApps(TestCase):
    def test_apps(self):
        self.assertEqual(Story9Config.name, 'Story_9') 
        self.assertEqual(apps.get_app_config('Story_9').name, 'Story_9')


class FormTest(TestCase):
    def test_form_is_valid(self):
        form_login = LoginForm(data={
            "username": "hhj",
            "password": "rahasia",
        })

        form_signup = SignupForm(data={
            "username": "hhj",
            "email": "haje@gmail.com",
            "password_first" : "rahasia",
            "password_again" : "rahasia",
        })
        self.assertTrue(form_signup.is_valid())

    def test_form_invalid(self):
        form_login = LoginForm(data={})
        self.assertFalse(form_login.is_valid())
        form_signup = SignupForm(data={})
        self.assertFalse(form_signup.is_valid())

    def test_form_signup_is_exist(self):
        form_signup = SignupForm(data={
            "username": "hhj",
            "email": "haje@gmail.com",
            "password_first" : "rahasia",
            "password_again" : "rahasia",
        })
        form_signup = SignupForm(data={
            "username": "hhj",
            "email": "haje@gmail.com",
            "password_first" : "rahasia",
            "password_again" : "rahasia",
        })
        self.assertTrue(form_signup.is_valid())



class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_login_ada(self):
        response = Client().get('/Story_9')
        self.assertEquals(response.status_code, 301)
    
    def test_url_signup_ada(self):
        response = Client().get('/Story_9/signup')
        self.assertEquals(response.status_code, 301)
    
    def test_url_logout_ada(self):
        response = Client().get('/Story_9/logout')
        self.assertEquals(response.status_code, 301)
    
    def test_url_berhasil_ada(self):
        response = Client().get('/Story_9/berhasil')
        self.assertEquals(response.status_code, 301)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_page = reverse("Story_9:login")
        self.signup_page = reverse("Story_9:signup")
        self.logout_page = reverse("Story_9:logout")
        self.user_new = User.objects.create_user("hhj","haje@gmail.com", password='rahasia')
        self.user_new.save()
        self.profile = Profile.objects.create(user=self.user_new)

    def test_GET_login(self):
        response = self.client.get(self.login_page, {
            'username': 'hhj', 
            'password':'rahasia'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_GET_signup(self):
        response = self.client.get(self.signup_page, {
            'username': 'hhj', 
            'email': 'haje@gmail.com', 
            'password_first':'rahasia', 
            'password_again':'rahasia'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')
    
    def test_GET_logout(self):
        response = self.client.get(self.logout_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'logout.html')

    def test_signup_login_post(self):
        #signup
        response = self.client.post(self.signup_page, data = {
            "username" : "hhj",
            "email" : "haje@gmail.com",
            "password_first" : "rahasia",
            "password_again" : "rahasia",
        })

        response = self.client.post(self.signup_page, data = {
            "username" : "hhj",
            "email" : "haje@gmail.com",
            "password_first" : "rahasia",
            "password_again" : "rahasi",
        })

        response = self.client.post(self.signup_page, data = {
            "username" : "hhj",
            "email" : "haje@gmail.com",
            "password_first" : "rahasia",
            "password_again" : "rahasia",
        })

        response = self.client.post(self.login_page,data ={
            "username" : "hhj",
            "password" : "rahasia"
        })
        self.assertEquals(response.status_code,302)

    def test_not_signedup_yet(self):
        response = self.client.post(self.login_page,data ={
            "username" : "hhaje",
            "password" : "rahasia"
        })
        self.assertEquals(response.status_code,200)

        
class ModelTest(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("hhj", password="rahasia")
        self.new_user.save()
        self.profile = Profile.objects.create(
            user = self.new_user
        )
        self.response = self.client.login(
            username = "hhj",
            password = "rahasia"
        )

    def test_instance_created(self):
        self.assertEqual(Profile.objects.count(),1)

    def test_instance_is_correct(self):
        self.assertEqual(Profile.objects.first().user,self.new_user)

    def test_to_string(self):
        self.assertIn("hhj",str(self.new_user.profile))
