from django.contrib import admin
from django.urls import path, include
from .views import login_page, signup_page, logout_page, berhasil

app_name = 'Story_9'

urlpatterns = [
    path('', login_page, name='login'),
    path('signup/', signup_page, name='signup'),
    path('logout/', logout_page, name='logout'),
    path('berhasil/',berhasil, name='berhasil')
] 
