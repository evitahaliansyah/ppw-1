$( function() {
    $( ".accordion" ).accordion({
      collapsible: true,
    });
    $(".accordion_head").click(function(){
      $(this).toggleClass("active");
      $(this).next().toggleClass("active");
    });

    $(".arrowup").click(function(){
      var $arrow = $(this).parent().parent().parent();
      $arrow.prev().before($arrow);
    });

    $(".arrowdown").click(function(){
      var $arrow = $(this).parent().parent().parent();
      $arrow.next().after($arrow);
    });
});