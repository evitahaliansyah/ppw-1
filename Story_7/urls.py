from django.urls import path, include
from .views import homepage

app_name='Story_7'
urlpatterns = [
    path('',homepage, name="Story_7")
]