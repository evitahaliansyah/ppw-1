from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage
from .apps import Story7Config

# Create your tests here.
class TestNamaApp(TestCase):
    def test_nama_app(self):
        self.assertEqual(Story7Config.name, 'Story_7')

class TestViews(TestCase):
    def test_event_using_template(self):
        response = Client().get('/Story_7/')
        self.assertTemplateUsed(response, 'home2.html')

    def test_menggunakan_template_berhasil(self):
        response = Client().get('/Story_7/')
        self.assertEqual(response.status_code, 200)

class TestUrls(TestCase):
    def test_apakah_berhasil_ada(self):
        found = resolve('/Story_7/')
        self.assertEqual(found.func, homepage)