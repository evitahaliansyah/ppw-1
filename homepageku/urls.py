from django.urls import path, include
from .views import kucing
from .views import anjing
from .views import ikan

appname = 'StoryEvita'

urlpatterns = [
    path('',kucing, name='home'),
    path('story1/',anjing),
    path('findme/', ikan)
]