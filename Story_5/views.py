from django.shortcuts import render, HttpResponseRedirect
from .models import Matkul
from .forms import MatkulForm

# Create your views here.
def matkul(request):
    matkulList = Matkul.objects.all()
    response = {
        'matkuls':matkulList
    }

    return render(request, "matkul.html", response)

def daftar_matkul(request):
    form = MatkulForm()
    if request.method == 'POST':
        form = MatkulForm(request.POST)

        Matkul.objects.create(
            nama = request.POST['nama'],
            dosen = request.POST['dosen'],
            sks = request.POST['sks'],
            deskripsi = request.POST['deskripsi'],
            semester = request.POST['semester'],
            ruang = request.POST['ruang']
        )

        return HttpResponseRedirect('/Story_5')
    
    response = {
        'form':form
    }
    return render(request, "form.html", response)

def hapus(request, id_matkul):
    Matkul.objects.filter(id=id_matkul).delete()
    return HttpResponseRedirect('/Story_5')