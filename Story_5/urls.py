from django.urls import path, include
from .views import matkul, daftar_matkul, hapus

urlpatterns = [
    path('',matkul),
    path('daftar/',daftar_matkul, name='daftar'),
    path('delete/<id_matkul>/', hapus, name='delete')
]