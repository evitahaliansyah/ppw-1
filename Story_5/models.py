from django.db import models

# Create your models here.
class Matkul(models.Model):
    nama = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField()
    deskripsi = models.CharField(max_length=100)
    semester = models.CharField(max_length=15)
    ruang = models.IntegerField()
