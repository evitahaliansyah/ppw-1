from django import forms
from .models import Matkul

class MatkulForm(forms.Form):
    nama = forms.CharField()
    dosen = forms.CharField()
    sks = forms.IntegerField()
    deskripsi = forms.CharField()
    semester = forms.CharField()
    ruang = forms.IntegerField()
    