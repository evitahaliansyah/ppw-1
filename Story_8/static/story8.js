$(window).on("load", function() {
    var input = "selection"
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + input,
            success: function(data) {
                $('#konten').html('')
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>"
                }
                $('#konten').append(result);
            },
            error: function(error) {
                alert("Books not found");
        }
    })
});

$(document).ready(function() {
    $("#input").on("keyup", function(e) {
        var input = e.currentTarget.value.toLowerCase()
        console.log(input)
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + input,
            success: function(data) {
                $('#konten').html('')
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>"
                }
                $('#konten').append(result);
            },
            error: function(error) {
                alert("Books not found");
            }
        })
    });
});