from django.test import TestCase, Client
from django.urls import resolve
from .views import landingPage, bookData
from .apps import Story8Config

# Create your tests here.
class TestNamaApp(TestCase):
    def test_nama_app(self):
        self.assertEqual(Story8Config.name, 'Story_8')

class TestViews(TestCase):
    def test_event_using_template(self):
        response = Client().get('/Story_8/')
        self.assertTemplateUsed(response, 'books.html')

    def test_menggunakan_template_berhasil(self):
        response = Client().get('/Story_8/')
        self.assertEqual(response.status_code, 200)

class TestUrls(TestCase):
    def test_fungsi_cari_jalan(self):
        response_get = Client().get("/Story_8/bookData", {"input": "a"})
        self.assertEqual(response_get.status_code, 200)