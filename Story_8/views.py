from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.
def landingPage(request):
    response = {}
    return render(request,'books.html',response)

def bookData(request):
    input = request.GET['input']
    url = "https://www.googleapis.com/books/v1/volumes?q=" + input
    response = requests.get(url)
    json_read = response.json()
    return JsonResponse(json_read)
