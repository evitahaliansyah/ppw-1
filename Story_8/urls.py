from django.urls import path
from .views import landingPage, bookData


appname = 'Story_8'

urlpatterns = [
   path('', landingPage, name = 'landingPage'),
   path('bookData', bookData, name='books_data'),
]
